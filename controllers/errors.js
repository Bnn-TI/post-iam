exports.unauthorized = function unauthorized(next, msg = 'Unauthorized') {
  next({
    status: 401,
    code: 'UNAUTHORIZED',
    message: msg
  })
}

exports.forbidden = function (next, msg = 'You are not allowed to perform this action', code = 'FORBIDDEN') {
  next({
    status: 403,
    code,
    message: msg
  })
}

exports.notFound = function (next, msg = 'Resource not found', code = 'NOT_FOUND') {
  next({
    headers: { 'Content-Type': 'application-json'},
    status: 404,
    code,
    message: msg
  })
}

exports.internalServerError = function (next, msg = 'There has been a problem with your request') {
  next({
    status: 500,
    code: 'INTERNAL_SERVER_ERROR',
    message: msg,
  })
}

exports.invalidCredentials = function (next) {
  next({
    status: 401,
    code: 'INVALID_CREDENTIALS',
    message: 'Invalid credentials'
  })
}

exports.userAlreadyRegistered = function (next) {
  next({
    status: 409,
    code: 'USER_ALREADY_REGISTERED',
    message: 'This user has already been registered'
  })
}

exports.clientAlreadyRegistered = function (next) {
  next({
    status: 409,
    code: 'CLIENT_ALREADY_REGISTERED',
    message: 'This client has already been registered'
  })
}

exports.invalidRequest = function (next, code = 'INVALID_REQUEST', msg = 'Invalid request') {
  next({
    status: 400,
    code,
    message: msg,
  })
}
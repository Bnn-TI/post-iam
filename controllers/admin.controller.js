const bcrypt = require('bcrypt')
const debug = require('debug')('microgi:iam:controllers:admin')

const {
  userAlreadyRegistered,
  clientAlreadyRegistered,
  internalServerError,
} = require('./errors')

const USERS_COLLECTION = process.env.USERS_COLLECTION || 'iam_users'
const CLIENTS_COLLECTION = process.env.CLIENTS_COLLECTION || 'iam_clients'
const DEFAULT_USER_REALM = process.env.DEFAULT_USER_REALM || 'iam_default_realm'

/**
 * Register new user
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
exports.registerNewUser = async function (req, res, next) {
  try {
    const {
      body
    } = req
    const microgiApi = res.locals.microgiApi.pApi
    const result = await microgiApi.users.registerUser(body)
    debug('result', result)
    if (result) {
      return res.status(201).send(
        result
      )
    } else {
      res.status(500).send({
        status: 500,
        code: 'INTERNAL ERROR',
        message: result.error
      })
    }
  } catch (error) {
    internalServerError(next, 'error')
  }
}
/**
 * 
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
exports.registerNewClient = async function (req, res, next) {
  try {
    debug('trying to register new client')
    const {
      db
    } = res.locals
    const {
      body
    } = req
    let {
      clientId,
      clientSecret
    } = body

    debug(`with clientId: ${clientId}`)


    debug(`looking for client ${clientId}`)
    const col = db.collection(CLIENTS_COLLECTION)
    const count = await col.countDocuments({
      _id: clientId
    })

    if (count > 0)
      return clientAlreadyRegistered(next)

    const result = await col.insertOne({
      _id: clientId,
      secret_hash: bcrypt.hashSync(clientSecret, 10),
      createdOn: new Date(),
    })

    const {
      n,
      ok
    } = result.result

    if (n && ok)
      return res.status(201).send({
        id: clientId
      })

    debug('register was not inserted. Error will be created')
    internalServerError(next, 'the client was not inserted')

  } catch (e) {
    next(e)
  }
}
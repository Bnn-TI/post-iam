/* eslint-disable no-unused-vars */
require('dotenv').config()
const express = require('express')
const debug = require('debug')('microgi:iam:server')

class Server {
  constructor() {
    this.port = process.env.PORT || 3000
    this.app = express()
  }

  async init() {
    debug('Initializing IAM Server')
    // ========================
    // ==== error handling ====
    // ========================
    this.app.use((err, req, res, next) => {
      debug('error mw is ', err)
      res
        .status(err.status || 500)
        .json({
          status: err.status || 500,
          code: err.code,
          error: err.error,
          msg: err.msg || err.message,
          description: err.description,
        })
    })
    debug('Initialized IAM Server')
  }

  async start() {
    await this.init()
    debug('Starting server on port', this.port)
    this.server = await this.app.listen(this.port)
      .on('listening', () => debug(`IAM Server started on port ${this.port}`))
      .on('error', err => debug('error is ', err))
    return this.server
  }

  async stop() {
    debug('Stopping server')
    try {
      await this.server.close()
    } catch (e) {
      debug(e)
      throw e
    }
  }
}

module.exports = Server
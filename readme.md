- Variables de entorno para probar local

export CORS_ENABLE=true
export LOGGED_IN_COOKIE_SECURED=false
export ACCESS_TOKEN_COOKIE_SECURED=false


- Para correr como servicion sin estar embebido se agrego el filtro de cors con las siguientes opciones:

File = ./lib/microgi-api.js

this.app.options('*',cors({
        origin: true,
        credentials: process.env.CORS_CREDENTIALS === 'true',
        allowedHeaders: [
          'Origin',
          'X-Requested-With',
          'X-HTTP-Method-Override',
          'Content-Type',
          'Accept',
          'Authorization'
        ],
      })) 

- Hay que agregar un indice en la base de datos en la colección de "iam_access_tokens"
para que los token se borren cuando expire la cookie:

name: expireAt_1

content: {
    "expireAt" : 1.0
}

options: expire after 0 seconds

      
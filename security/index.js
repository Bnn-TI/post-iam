const jwt = require('jsonwebtoken')
const debug = require('debug')('microgi:iam:security')
const defaultConfig = require('./config')
const { unauthorized, forbidden } = require('../controllers/errors')
const ACCESS_TOKENS_COLLECTION =
  process.env.ACCESS_TOKENS_COLLECTION || 'iam_access_tokens'
const {
  jwtSignSecret,
  generateCookies,
  clearCookies
} = require('../utils/security')
const disableSecurity = process.env.DISABLE_SECURITY === 'true'
const DEFAULT_ACCESS_TOKEN_EXPIRATION =
  Number.parseInt(process.env.DEFAULT_ACCESS_TOKEN_EXPIRATION || '1000') * 1000
const RETURN_ACCESS_TOKEN_IN_COOKIE =
  process.env.RETURN_ACCESS_TOKEN_IN_COOKIE || 'false'
const ACCESS_TOKEN_COOKIE_NAME =
  process.env.ACCESS_TOKEN_COOKIE_NAME || 'microgi_cookie'
const COOKIE_SECRET_KEY = process.env.COOKIE_SECRET_KEY
const LOGGED_IN_COOKIE_NAME =
  process.env.LOGGED_IN_COOKIE_NAME || 'microgi_cookie_logged_in'

let config = null
const security = function(myConfig = defaultConfig) {
  debug('mounting security filter')
  config = myConfig
  return async function(req, res, next) {
    debug(
      `validating request in security filter disableSecurity [${disableSecurity}]`
    )
    res.header('Access-Control-Allow-Credentials', true)
    res.header('Access-Control-Allow-Origin', req.headers.origin)
    res.header(
      'Access-Control-Allow-Methods',
      'GET,PUT,POST,DELETE,UPDATE,OPTIONS'
    )
    res.header(
      'Access-Control-Allow-Headers',
      'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept'
    )
    if (disableSecurity) return next()

    if (res.locals.microgiConfig.security.before) {
      const params = {}
      await res.locals.microgiConfig.security.before(params, req, res, next)
    }

    // Cookies that have not been signed
    debug('Cookies: ', req.cookies)
    // Cookies that have been signed
    debug('Signed Cookies: ', req.signedCookies)
    let access = false
    debug('validating auth token')
    const authHeader = req.headers['authorization']
    const cookie = COOKIE_SECRET_KEY
      ? req.signedCookies[ACCESS_TOKEN_COOKIE_NAME]
      : req.cookies[ACCESS_TOKEN_COOKIE_NAME]
    const destructuredHeader = authHeader ? authHeader.split(' ') : []
    let params = {}

    if (
      (!authHeader && !cookie) ||
      (authHeader && destructuredHeader[0].toLocaleLowerCase() === 'basic')
    )
      access = evaluateRequest(req, null, config)
    else {
      if (
        !(
          cookie ||
          (authHeader &&
            destructuredHeader[0].toLocaleLowerCase() === 'bearer' &&
            destructuredHeader.length === 2)
        )
      )
        return unauthorized(next, 'INVALID_TOKEN')

      debug('extracting token')
      let token = authHeader ? destructuredHeader[1] : cookie

      debug('validating token exists in db')
      const actualToken = await res.locals.db
        .collection(ACCESS_TOKENS_COLLECTION)
        .findOne({
          token: token
        })

      if (!actualToken) {
        clearCookies(req.headers, res)
        return forbidden(next)
      }

      debug('validating token')
      let decoded
      try {
        decoded = jwt.verify(token, jwtSignSecret)
      } catch (e) {
        return unauthorized(next, 'INVALID_TOKEN')
      }

      debug('getting username')
      let username = decoded.sub
      let roles = decoded.roles
      if (!username) {
        return unauthorized(next, 'INVALID_TOKEN')
      }

      access = evaluateRequest(req, decoded, config)

      const expireAt = new Date(Date.now() + DEFAULT_ACCESS_TOKEN_EXPIRATION)
      let keyId = actualToken._id

      debug('updating token document with new expiredAt')
      await res.locals.db.collection(ACCESS_TOKENS_COLLECTION).updateOne(
        {
          _id: keyId
        },
        {
          $set: {
            token,
            expiresIn: DEFAULT_ACCESS_TOKEN_EXPIRATION,
            expireAt: expireAt
          }
        }
      )

      if (
        RETURN_ACCESS_TOKEN_IN_COOKIE &&
        RETURN_ACCESS_TOKEN_IN_COOKIE.toLocaleLowerCase().trim() !== 'false'
      ) {
        const cookieValue = token
        generateCookies(cookieValue, expireAt, req.headers, res)
      }

      res.locals.username = username
      res.locals.token = token
      res.locals.roles = roles

      params.username = username
      params.token = token
      params.roles = roles
    }

    if (access) {
      if (res.locals.microgiConfig.security.after) {
        await res.locals.microgiConfig.security.after(params, req, res, next)
      }
      return next()
    } else {
      return unauthorized(next, 'UNAUTHORIZED')
    }
  }
}

function evaluateRequest(req, auth, config) {
  const method = (req.method || '').trim().toUpperCase()
  const url = (req.url || '').trim().toLowerCase()
  // look for the first rule that matches
  for (let idx in config.acls) {
    let acl = config.acls[idx]
    debug('acl ', acl)
    if (url.match(acl.url) && method === acl.method) {
      // found matching rule
      // checking if no auth
      if (acl.any) {
        return acl.allow
      }
      // checking any role rule on auth
      if (auth && acl.anyRole && auth.roles) {
        let matchRole = acl.anyRole.some(r => auth.roles.includes(r))
        debug('matchRole ', JSON.stringify(matchRole))
        if (matchRole) {
          return acl.allow
        }
        return false
      }
    }
  }

  return config.defaultAccess
}

module.exports = security

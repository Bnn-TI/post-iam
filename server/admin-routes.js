const express = require('express')
const debug = require('debug')('microgi:iam:server:admin')
const adminControllers = require('../controllers/admin.controller')
const defaultConfig = require('./admin-routes-config')
const MOUNT_ADMIN_ENDPOINTS = process.env.MOUNT_ADMIN_ENDPOINTS

const init = function (config = defaultConfig) {
  const router = express.Router()
  if (MOUNT_ADMIN_ENDPOINTS && MOUNT_ADMIN_ENDPOINTS.toLocaleLowerCase().trim() !== 'false') {
    debug('mounting admin routes')
    if (config.users) {
      if (config.users.mount) {
        router.post(config.users.endpoint, adminControllers.registerNewUser)
      }
    }
    if (config.clients) {
      if (config.clients.mount) {
        router.post(config.clients.endpoint, adminControllers.registerNewClient)
      }
    }
  }
  return router
}

module.exports = init
const debug = require('debug')('microgi:iam:utils:security')
const base64Secret = process.env.TOKEN_SIGN_SECRET
const secret = String(Buffer.from(process.env.TOKEN_SIGN_SECRET, 'base64'), 'UTF-8')
const randomstring = require('randomstring')
const cookieParser = require('cookie-parser')

const ACCESS_TOKEN_COOKIE_NAME =
    process.env.ACCESS_TOKEN_COOKIE_NAME || 'microgi_cookie'
const ACCESS_TOKEN_COOKIE_HTTP_ONLY = (process.env.ACCESS_TOKEN_COOKIE_HTTP_ONLY || 'true') === 'true'
const ACCESS_TOKEN_COOKIE_SECURED = (process.env.ACCESS_TOKEN_COOKIE_SECURED || 'true') === 'true'
const LOGGED_IN_COOKIE_NAME = process.env.LOGGED_IN_COOKIE_NAME || 'microgi_cookie_logged_in'
const LOGGED_IN_COOKIE_HTTP_ONLY = (process.env.LOGGED_IN_COOKIE_HTTP_ONLY || 'false') === 'true'
const LOGGED_IN_COOKIE_SECURED = (process.env.LOGGED_IN_COOKIE_SECURED || 'true') === 'true'
const COOKIE_SECRET_KEY = process.env.COOKIE_SECRET_KEY
const COOKIE_DOMAIN = process.env.COOKIE_DOMAIN
const COOKIE_SAMESITE = process.env.COOKIE_SAMESITE || 'None'

exports.jwtSignSecret = secret

/**
 * validate tieme of expiration of token
 *  @param {number} interval
 *  @param {date} past
 *  @param {date} today
 */

exports.validateValidityForToken = function (interval, past, today) {
    const created = new Date(past)
    const now = new Date(today)
    const createdWithInterval = new Date(created.getTime() + (parseInt(interval) * 1000))
    return createdWithInterval.getTime() >= now.getTime()
}

/**
 * validate a field is required
 * @param {string} value
 */
exports.isNotDefined = function (value) {
    return typeof value === 'undefined' || value === null
}
/**
 * Generate a token for password or confirm user account
 * @params {string} type :if true is restore prefix else new prefix
 */

exports.generateTokenUser = async (type) => {
    let passwordType = process.env.TOKEN_PASSWORD_PREFIX_NEW || ''
    if (type)
        passwordType = process.env.TOKEN_PASSWORD_PREFIX_RESTORE || ''

    const rmString = randomstring.generate({
        length: parseInt(process.env.TOKEN_PASSWORD_LENGTH || 16),
        charset: 'alphanumeric',
    })

    return {
        created: new Date(),
        data: passwordType + rmString
    }
}

exports.errorCodes = {
    password: {
        REQUIRED: 'FIELD_IS_REQUIRED',
        SAME_PASSWORDS: 'SAME_PASSWORDS', // 'The passwords must be the same',
        USERNAME_ERROR: 'PASSWORD_MUST_NOT_CONTAIN_USERNAME', // 'The password must not contain the username',
        EMAIL_ERROR: 'PASSWORD_MUST_NOT_CONTAIN_EMAIL', // 'The password must not contain the email',
        CHARS_ERROR: 'PASSWORD_MUST_CONTAIN_A_NUMBER', // 'The password must contain at least one numeric character',
        LEAST_A_LETER: 'PASSWORD_MUST_CONTAIN_A_LETTER', // 'The password must contain at least one letter',
        MIN_LENGTH: 'PASSWORD_CHARS_LENGTH', // 'The password must contain at least 8 characters in length',
        UNTIL_TWO_LETTER: 'PASSWORD_CONSECUTIVE_LETTER', // 'The password can only contain up to two consecutive letters',
        UNTIL_TWO_SAME_LETTER: 'PASSWORD_CONSECUTIVE_SAME_LETTER', // 'The password can only contain up to two consecutive equal letters',
        UNTIL_TWO_NUMBER: 'PASSWORD_CONSECUTIVE_NUMBERS', // 'The password can only contain up to two consecutive numbers',
        UNTIL_TWO_SPECIAL_CHARS: 'PASSWORD_CONSECUTIVE_SPECIAL_CHARS', // 'The password can only contain up to two consecutive special characters, including accents and virgins (~)',
        SAME_PASSWORD: 'PASSWORD_SAME', // 'The new password is the same as the previous one',
        NOT_VERIFIED: 'NOT_VERIFIED', // 'The email is not verified',
        VERIFIED: 'VERIFIED', // 'The email is verified',
        NOT_REGISTER: 'EMAIL_NOT_REGISTER', // 'The email is not register',
        KEY_INVALID: 'KEY_IS_INVALID', // 'The key is invalid',
        KEY_EXPIRED: 'KEY_EXPIRED', // 'The key is expired',
        INACTIVE: 'USER_IS_INACTIVE', // 'You don\'t have permission',
        INVALID_PASSWORD: 'INVALID_PASSWORD_FORMAT', // 'invalid password'
        FORBIDDEN_PASSWORD: 'FORBIDDEN_PASSWORD', //'You password don\'t have permission',
    },
    DUPLICATED: 'REGISTER_DUPLICATED',
    EXPIRED: 'RESOURCE_IS_EXPIRED',
    UPDATE: 'UPDATE_ERROR',
    CREATE: 'CREATE_ERROR',
    NOT_FOUND: 'RESOURCE_NOT_FOUND',
    INTERNAL: 'INTERNAL_SERVER_ERROR',
    BAD_REQUEST: 'BAD_REQUEST',
    filter: {
        SYNTAX_ERROR: 'FILTER_SYNTAX_ERROR'
    }
}

function getCookie(ctxCookie, cname, decode) {
    const name = cname + '='
    debug('headers.cookie', ctxCookie)
    debug('cname', cname)

    const decodedCookie = ctxCookie

    if (decode) {
        decodedCookie = decodeURIComponent(ctxCookie.cookie)
    }

    const ca = decodedCookie.split(';')
    for (var i = 0; i < ca.length; i++) {
        let c = ca[i]
        while (c.charAt(0) == ' ') {
            c = c.substring(1)
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length)
        }
    }
    return null
}

function getDomain(headers) {
    debug('get domain process', headers)
    let domain = ''
    if (COOKIE_DOMAIN) {
        debug('COOKIE_DOMAIN exists', COOKIE_DOMAIN)
        domain = COOKIE_DOMAIN
    } else {
        debug('COOKIE_DOMAIN not exists')
        const splitPort = headers.origin.split(':')
        debug('splitPort', splitPort)
        if (splitPort.length > 0) {
            const domainSplit = splitPort[1].split('/')
            debug('domain init', domain)
            domain = domainSplit[2]
            const dotDomain = domainSplit[2].split('.')
            debug('dotDomain', dotDomain)
            if (dotDomain.length > 1) {
                dotDomain.splice(0, 1)
                debug('dotDomain', dotDomain)
                domain = dotDomain.join('.')
            }
        }
        debug('domain finish', domain)
    }
    debug('finish domain process', domain)
    return domain
}

exports.generateCookies = function (cookieValue, expireAt, headers, res) {
    debug('into generate cookies origin', headers)

    const domain = getDomain(headers)

    res.cookie(
        ACCESS_TOKEN_COOKIE_NAME, cookieValue, {
            expires: expireAt,
            // maxAge: DEFAULT_ACCESS_TOKEN_EXPIRATION * 1000,
            httpOnly: ACCESS_TOKEN_COOKIE_HTTP_ONLY,
            secure: ACCESS_TOKEN_COOKIE_SECURED,
            signed: true,
            domain: `${domain}`,
            sameSite: COOKIE_SAMESITE,
        })
    res.cookie(
        LOGGED_IN_COOKIE_NAME, { logged: true, domain, token: cookieValue }, {
            expires: expireAt,
            // maxAge: DEFAULT_ACCESS_TOKEN_EXPIRATION * 1000,
            httpOnly: LOGGED_IN_COOKIE_HTTP_ONLY,
            secure: LOGGED_IN_COOKIE_SECURED,
            signed: true,
            domain: `${domain}`,
            sameSite: COOKIE_SAMESITE,
        })
    debug('generate cookies finished')

}

exports.clearCookies = function (origin, res) {
    debug('clear cookies init')
    const domain = getDomain(origin)

    res.clearCookie(
        ACCESS_TOKEN_COOKIE_NAME, {
            httpOnly: ACCESS_TOKEN_COOKIE_HTTP_ONLY,
            secure: ACCESS_TOKEN_COOKIE_SECURED,
            signed: true,
            domain: `${domain}`,
            sameSite: COOKIE_SAMESITE,
        })
    res.clearCookie(
        LOGGED_IN_COOKIE_NAME, {
            httpOnly: LOGGED_IN_COOKIE_HTTP_ONLY,
            secure: LOGGED_IN_COOKIE_SECURED,
            signed: true,
            domain: `${domain}`,
            sameSite: COOKIE_SAMESITE,
        }
    )

    debug('clear cookies finished')
}
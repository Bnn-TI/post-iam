/* eslint-disable no-unused-vars */
require('dotenv').config()
const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const debug = require('debug')('microgi:iam:api:microgi-api')
const user = require('../server/routes')
const admin = require('../server/admin-routes')
const routesConfig = require('../server/routes-config')
const adminRoutesConfig = require('../server/admin-routes-config')
const programaticApi = require('./services')
const securityConfig = require('../security/config')
const security = require('../security')

const router = express.Router()
const COOKIE_SECRET_KEY = process.env.COOKIE_SECRET_KEY

const defaultConfig = {
  security: securityConfig,
  methods: {
    user: routesConfig,
    admin: adminRoutesConfig
  }
}

const createApi = function(server, config = defaultConfig) {
  const microgiIAM = new MicrogiIAM(server, config)
  return microgiIAM
}

class MicrogiIAM {
  constructor(server, config = defaultConfig) {
    this.app = server
    this.config = config
  }

  async init() {
    debug('init')
    const pApi = await programaticApi(this.app)
    this.pApi = pApi
    this.db = this.pApi.db
    this.app.db = this.db
    //debug('this.app.db', this.app.db)


    if (process.env.CORS_ENABLE === 'true') {
      //if microgi is not embeded with other system need use options cors middleware to options request
      this.app.options('*',cors({
        origin: true,
        credentials: process.env.CORS_CREDENTIALS === 'true',
        allowedHeaders: [
          'Origin',
          'X-Requested-With',
          'X-HTTP-Method-Override',
          'Content-Type',
          'Accept',
          'Authorization'
        ],
      })) //cors options request

      this.app.use(
        cors({
          credentials: process.env.CORS_CREDENTIALS === 'true',
          preflightContinue: true,
          allowedHeaders: [
            'Origin',
            'X-Requested-With',
            'X-HTTP-Method-Override',
            'Content-Type',
            'Accept',
            'Authorization'
          ],
          methods: ['GET', 'PUT', 'POST', 'DELETE', 'UPDATE', 'OPTIONS', 'HEAD']
        })
      )
    }

    this.app.use(
      bodyParser.urlencoded({
        extended: false
      })
    )

    this.app.use(bodyParser.json())

    if (!COOKIE_SECRET_KEY) {
      debug('NO COOKIE_SECRET_KEY configured!!!!')
    }
    this.app.use(cookieParser(COOKIE_SECRET_KEY))
    this.app.use((req, res, next) => {
      res.locals.db = this.db
      res.locals.microgiConfig = this.config
      res.locals.microgiApi = this
      next()
    })

    this.app.use(security(this.config.security))
    debug('end init')
  }

  async middleware() {
    await this.init()
    if (this.config.methods.user.mount) {
      router.use(
        this.config.methods.user.endpoint,
        user(this.config.methods.user)
      )
    }
    if (this.config.methods.admin.mount) {
      router.use(
        this.config.methods.admin.endpoint,
        admin(this.config.methods.admin)
      )
    }
    this.app.use(router)
    return this
  }
}

exports.defaultConfig = defaultConfig
exports.createApi = createApi

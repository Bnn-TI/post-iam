const debug = require('debug')('microgi:iam:controllers:manage-user')

const {
  invalidRequest,
  internalServerError,
  unauthorized,
  forbidden,
  notFound
} = require('./errors')
const { errorCodes, isNotDefined } = require('../utils/security')

/**
 * validate fields
 * @param {string} value
 */
function validateDefinedFieldsPassword(
  username,
  key,
  password,
  passwordConfirm,
  change
) {
  if (isNotDefined(username))
    return {
      isValid: false,
      field: 'username'
    }
  if (!change) {
    if (isNotDefined(key))
      return {
        isValid: false,
        field: 'key'
      }
  }
  if (isNotDefined(password))
    return {
      isValid: false,
      field: 'password'
    }
  if (isNotDefined(passwordConfirm))
    return {
      isValid: false,
      field: 'passwordConfirm'
    }
  return {
    isValid: true
  }
}

/**
 *  Assign password to User
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */

/**
 * Seelect a http status code corresponding
 * @param {string} error
 */

function httpErrorResponse(next, error) {
  //  function httpErrorResponse(next, error){

  debug('http', error)
  switch (error) {
  case errorCodes.password.REQUIRED:
  case errorCodes.password.SAME_PASSWORDS:
  case errorCodes.password.USERNAME_ERROR:
  case errorCodes.password.EMAIL_ERROR:
  case errorCodes.password.CHARS_ERROR:
  case errorCodes.password.LEAST_A_LETER:
  case errorCodes.password.MIN_LENGTH:
  case errorCodes.password.UNTIL_TWO_LETTER:
  case errorCodes.password.UNTIL_TWO_SAME_LETTER:
  case errorCodes.password.UNTIL_TWO_NUMBER:
  case errorCodes.password.UNTIL_TWO_SPECIAL_CHARS:
  case errorCodes.password.SAME_PASSWORD:
  case errorCodes.password.KEY_INVALID:
  case errorCodes.password.INVALID_PASSWORD:
    return invalidRequest(next, errorCodes.password.INVALID_PASSWORD, error)
  case errorCodes.password.NOT_REGISTER:
    return unauthorized(next, error)
  case errorCodes.password.KEY_EXPIRED:
  case errorCodes.password.NOT_VERIFIED:
  case errorCodes.password.VERIFIED:
  case errorCodes.password.INACTIVE:
  case errorCodes.EXPIRED:
  case errorCodes.password.FORBIDDEN_PASSWORD:
    return forbidden(next, error)
  case errorCodes.NOT_FOUND:
    return notFound(next)
  case errorCodes.UPDATE:
    return internalServerError(next, error)
  }
}

/**
 * assign password for an user
 *  @params {*} req
 *  @params {*} res
 *  @params {*} next
 */
const regenerateConfirmKey = async function(req, res, next) {
  try {
    if (res.locals.microgiApi.config.methods.user.regenerateConfirmKey.before) {
      if (req.body) {
        if (req.body.username) {
          const params = {
            username: req.body.username
          }
          const callbackResult = await res.locals.microgiApi.config.methods.user.regenerateConfirmKey.before(
            params,
            req,
            res,
            next
          )
          debug('before callback regenerateConfirmKey', callbackResult)
          if (callbackResult && callbackResult.error) {
            debug('error regenerateConfirmKey callback')
            return invalidRequest(
              next,
              errorCodes.password.KEY_INVALID,
              callbackResult.error
            )
          }
        }
      }
    }

    const {
      body: { username }
    } = req

    const pApi = res.locals.microgiApi.pApi
    const result = await pApi.users.regenerateConfirmKey(username)
    if (result.data) {
      debug('regenerate confirm key correcty', result)
      if (
        res.locals.microgiApi.config.methods.user.regenerateConfirmKey.after
      ) {
        const user = {
          username: result.data.username
        }
        const params = {
          user,
          key: result.data.confirmKey
        }
        const callbackResult = await res.locals.microgiApi.config.methods.user.regenerateConfirmKey.after(
          params,
          req,
          res,
          next
        )
        debug('after callback regenerateConfirmKey', callbackResult)
        if (callbackResult && callbackResult.error) {
          debug('error regenerateConfirmKey callback')
          return invalidRequest(
            next,
            errorCodes.password.KEY_INVALID,
            callbackResult.error
          )
        }
        debug('after callback regenerateConfirmKey success')
      }
      return res.status(200).json({
        status: 200,
        msg: 'success'
      })
    } else {
      debug('regenerate confirm key incorrectly')
      return httpErrorResponse(next, result.error)
    }
  } catch (e) {
    debug('Error', e)
    return internalServerError(next, e)
  }
}

/**
 * Valida token de password y redirige a nuevo link
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const verifyAccount = async function(req, res, next) {
  try {
    if (res.locals.microgiApi.config.methods.user.confirm.before)
      await res.locals.microgiApi.config.methods.user.confirm.before(
        {},
        req,
        res,
        next
      )
    debug('get verifyAccount email', req.query.e)
    const {
      query: { e, k }
    } = req
    debug(`validate Token ${req.query.e} -- ${req.query.k}`)
    const pApi = res.locals.microgiApi.pApi
    const result = await pApi.users.verifyAccount(req.query.e, req.query.k)
    debug('verifyAccount', result)
    if (!result.error) {
      let params = {
        email: result.data.username,
        confirmKey: result.data.token
      }
      if (res.locals.microgiApi.config.methods.user.confirm.after) {
        const link = await res.locals.microgiApi.config.methods.user.confirm.after(
          params,
          req,
          res,
          next
        )
        debug('callback verifyAccount', link)
        res.redirect(link)
      }
    } else {
      debug('Token error')
      return httpErrorResponse(next, result.error)
    }
  } catch (error) {
    debug('Error', error)
    return internalServerError(next, error)
  }
}

/**
 * assign password for an user
 *  @params {*} req
 *  @params {*} res
 *  @params {*} next
 */
const assignPassword = async function(req, res, next) {
  try {
    if (res.locals.microgiApi.config.methods.user.passwordAssign.before) {
      if (req.body) {
        if (req.body.password) {
          const params = {
            password: req.body.password,
            username: req.body.username
          }
          const error = await res.locals.microgiApi.config.methods.user.passwordAssign.before(
            params,
            req,
            res,
            next
          )
          debug('before callback assignpassword success')
          if (error) {
            debug('error assignpassword callback')
            return invalidRequest(
              next,
              errorCodes.password.INVALID_PASSWORD,
              error
            )
          }
        }
      }
    }

    const {
      body: { username, key, password, passwordConfirm }
    } = req

    const validFields = validateDefinedFieldsPassword(
      username,
      key,
      password,
      passwordConfirm
    )
    if (validFields.isValid) {
      debug('validation fiels pass')
      const pApi = res.locals.microgiApi.pApi
      const result = await pApi.users.assignPassword(
        username,
        key,
        password,
        passwordConfirm
      )
      if (result.data) {
        debug('assign password correcty', result)
        if (res.locals.microgiApi.config.methods.user.passwordAssign.after) {
          const user = {
            username: result.data.user.username
          }
          const params = {
            user,
            key: result.data.key,
            passwordHash: result.data.passwordHash
          }
          await res.locals.microgiApi.config.methods.user.passwordAssign.after(
            params,
            req,
            res,
            next
          )
          debug('after callback assignpassword success')
        }
        return res.status(200).json({
          status: 200,
          msg: 'success'
        })
      } else {
        debug('assign password incorrectly')
        return httpErrorResponse(next, result.error)
      }
    } else {
      debug('fields not defined')
      return invalidRequest(
        next,
        'BAD_REQUEST',
        `The field ${validFields.field} is required`
      )
    }
  } catch (e) {
    debug('Error', e)
    return internalServerError(next, e)
  }
}

const initPassword = async function(req, res, next) {
  try {
    if (res.locals.microgiApi.config.methods.user.passwordInit.before) {
      const params = {}
      await res.locals.microgiApi.config.methods.user.passwordInit.before(
        params,
        req,
        res,
        next
      )
    }

    const {
      body: { username }
    } = req

    if (isNotDefined(username)) {
      debug('username is not defined')
      return httpErrorResponse(next, errorCodes.password.REQUIRED)
    }
    debug('username defined')
    const pApi = res.locals.microgiApi.pApi
    const result = await pApi.users.initPassword(username)
    if (!result.data) {
      debug('init password error', result)
      return httpErrorResponse(next, result.error)
    }
    debug('init password success', result)
    if (res.locals.microgiApi.config.methods.user.passwordInit.after) {
      const user = {
        username: result.data.user.username
      }
      const params = {
        user,
        key: result.data.token.data
      }
      await res.locals.microgiApi.config.methods.user.passwordInit.after(
        params,
        req,
        res,
        next
      )
      debug('after callback initPassword success')
    }
    debug('after callback')
    return res.status(201).json({
      status: 201,
      code: 'SUCCESS',
      message: 'CREATED'
    })
  } catch (e) {
    debug('e', e)
    return internalServerError(next, 'ERROR_INIT_PASSWORD')
  }
}

/**
 *
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const changePassword = async function(req, res, next) {
  try {
    if (res.locals.microgiApi.config.methods.user.passwordChange.before) {
      debug('before', req.password)
      if (req.body) {
        if (req.body.password) {
          const params = {
            password: req.body.password,
            username: res.locals.username
          }
          const error = await res.locals.microgiApi.config.methods.user.passwordChange.before(
            params,
            req,
            res,
            next
          )
          debug('before callback changepassword success')
          if (error) {
            debug('error password change callback')
            return invalidRequest(
              next,
              errorCodes.password.INVALID_PASSWORD,
              error
            )
          }
        }
      }
    }

    const pApi = res.locals.microgiApi.pApi

    const {
      body: { password, passwordConfirm, passwordCurrently }
    } = req

    const username = res.locals.username

    const validFields = validateDefinedFieldsPassword(
      username,
      null,
      password,
      passwordConfirm,
      true
    )
    if (validFields.isValid) {
      debug('validation fiels pass')
      const result = await pApi.users.changePassword(
        username,
        password,
        passwordConfirm,
        passwordCurrently
      )
      debug('result', result)
      if (result.data) {
        debug('change password correcty')
        if (res.locals.microgiApi.config.methods.user.passwordChange.after) {
          const user = {
            username: result.data.user.username
          }

          const params = {
            user,
            key: result.data.key,
            passwordHash: result.data.passwordHash
          }
          await res.locals.microgiApi.config.methods.user.passwordChange.after(
            params,
            req,
            res,
            next
          )
          debug('after callback changePassword success')
        }
        return res.status(200).send({
          status: 200,
          msg: 'success'
        })
      } else {
        debug('assign password incorrectly')
        return httpErrorResponse(next, result.error)
      }
    } else {
      debug('fields not defined')
      return invalidRequest(
        next,
        'BAD_REQUEST',
        `The field ${validFields.field} is required`
      )
    }
  } catch (e) {
    debug('Error', e)
    return internalServerError(next, e)
  }
}
/**
 *  get a information  about user by emal
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */

const getInfoUserByUsername = async function(req, res, next) {
  try {
    if (
      res.locals.microgiApi.config.methods.user.getInfoUserByUsername.before
    ) {
      await res.locals.microgiApi.config.methods.user.getInfoUserByUsername.before(
        {},
        req,
        res,
        next
      )
      debug('before callback getInfoUserByUsername success')
    }

    const pApi = res.locals.microgiApi.pApi

    const result = await pApi.users.getInfoUserByUsername(res.locals.username)
    debug('result', result)
    if (result.data) {
      debug('getData correct')
      if (
        res.locals.microgiApi.config.methods.user.getInfoUserByUsername.after
      ) {
        const params = {}
        await res.locals.microgiApi.config.methods.user.getInfoUserByUsername.after(
          params,
          req,
          res,
          next
        )
        debug('after callback getInfoUserByUsername success')
      }
      return res.status(200).send(result.data)
    } else {
      debug('get data error incorrectly')
      return httpErrorResponse(next, result.error)
    }
  } catch (e) {
    debug('Error', e)
    return internalServerError(next, e)
  }
}

/**
 * get all users
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */

async function getAllUsers (req, res, next) {
  try {
    const {
      query,
    } = req
    const microgiApi = res.locals.microgiApi.pApi
    const result = await microgiApi.users.getAllUsers(query)
    if (result) {
        if(!result.data)
          return result.error ? invalidRequest(next, 'BAD_REQUEST', result.error) : notFound(next)
          
        return res.status(200).send(result.data)
    } else return internalServerError(next, errorCodes.INTERNAL)
  } catch (error) {
    internalServerError(next, error)
  }
}

/**
 * update an user info
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */

async function updateUser (req, res, next) {
  try {
    const {
      body,
    } = req
    debug('body',body)
    const microgiApi = res.locals.microgiApi.pApi
    const result = await microgiApi.users.updateUser(body)
    if (result) {
      if(!result.data)
        return internalServerError(next, result.error)

      return res.status(200).send(
          {
            id: result.data.user._id,
            username: result.data.user.username
          }
      )
    } else return internalServerError(next, errorCodes.INTERNAL)
  } catch (error) {
    internalServerError(next, error)
  }
}
/**
 * deletes an user logicly
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */

async function deleteUser (req, res, next) {
  try {
    const {
      body,
    } = req
    debug('body',body)
    const microgiApi = res.locals.microgiApi.pApi
    const result = await microgiApi.users.updateUser(body, true)
    if (result) {
      if(!result.data)
        return internalServerError(next, result.error)

      return res.status(200).send({
          id: result.data.user._id,
          username: result.data.user.username
      })    
    } else return internalServerError(next, errorCodes.INTERNAL)
  } catch (error) {
    internalServerError(next, error)
  }
}

/**
 * creates an user
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */

async function createUser (req, res, next) {
  try {
    const {
      body,
    } = req
    const microgiApi = res.locals.microgiApi.pApi
    debug('body',body)
    const result = await microgiApi.users.registerUser(body)
    if (result) {
      if(!result.data)
        return internalServerError(next, result.error)
      
      return res.status(200).send(result.data)
    } else return internalServerError(next, result.error)
  } catch (error) {
    internalServerError(next, error)
  }
}

exports.regenerateConfirmKey = regenerateConfirmKey
exports.verifyAccount = verifyAccount
exports.initPassword = initPassword
exports.assignPassword = assignPassword
exports.changePassword = changePassword
exports.getInfoUserByUsername = getInfoUserByUsername
exports.getAllUsers = getAllUsers
exports.updateUser = updateUser
exports.deleteUser = deleteUser
exports.createUser = createUser

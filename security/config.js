const config = {
    before: async function (params, req, res, next) { },
    after: async function (params, req, res, next) { },
    defaultAccess: false,
    acls: [{
        method: 'POST',
        url: /\/admin\/users/,
        allow: true,
        anyRole: ['admin', 'system'],
    }, {
        method: 'POST',
        url: /\/admin\/clients/,
        allow: true,
        anyRole: ['admin', 'system'],
    }, {
        method: 'POST',
        url: /\/token/,
        allow: true,
        any: true,
    }, {
        method: 'DELETE',
        url: /\/logout/,
        allow: true,
        any: true,
    },{
        method: 'DELETE',
        url: /\/deleteuser/,
        allow: true,
        anyRole: ['admin', 'system', 'user'],
    }, {
        method: 'POST',
        url: /\/regenerateconfirmkey/,
        allow: true,
        any: true,
    }, {
        method: 'GET',
        url: /\/confirm/,
        allow: true,
        any: true,
    }, {
        method: 'GET',
        url: /\/getinfouserbyusername/,
        allow: true,
        anyRole: ['admin', 'system', 'user'],
    }, {
        method: 'GET',
        url: /\/getallusers/,
        allow: true,
        anyRole: ['admin', 'system', 'user'],
    },{
        method: 'POST',
        url: /\/createuser/,
        allow: true,
        anyRole: ['admin', 'system', 'user'],
    },{
        method: 'POST',
        url: /\/updateuser/,
        allow: true,
        anyRole: ['admin', 'system', 'user'],
    },{
        method: 'POST',
        url: /\/initpassword/,
        allow: true,
        any: true,
    }, {
        method: 'POST',
        url: /\/assignpassword/,
        allow: true,
        any: true,
    }, {
        method: 'POST',
        url: /\/changepassword/,
        allow: true,
        anyRole: ['admin', 'system', 'user'],
    }, {
        method: 'POST',
        url: /.*/,
        allow: true,
        anyRole: ['admin', 'system', 'user'],
    }, {
        method: 'DELETE',
        url: /.*/,
        allow: true,
        anyRole: ['admin', 'system', 'user'],
    }, {
        method: 'PUT',
        url: /.*/,
        allow: true,
        anyRole: ['admin', 'system', 'user'],
    }, {
        method: 'PATCH',
        url: /.*/,
        allow: true,
        anyRole: ['admin', 'system', 'user'],
    }, {
        method: 'GET',
        url: /.*/,
        allow: true,
        any: true,
    }, {
        method: 'OPTIONS',
        url: /.*/,
        allow: true,
        any: true,
    }],
}

module.exports = config
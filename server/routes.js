const express = require('express')
const debug = require('debug')('microgi:iam:server:routes')

const controller = require('../controllers/authorize.controller')
const manageUserController = require('../controllers/manage.user.controller')
const defaultConfig = require('./routes-config')

const init = function (config = defaultConfig) {
  debug('mounting routes')
  const router = express.Router()
  if (config.token && config.token.mount) 
    router.post(config.token.endpoint, controller.authorize)
    
  if (config.logout && config.logout.mount) 
    router.delete(config.logout.endpoint, controller.logout)
  
  if (config.passwordAssign && config.passwordAssign.mount)
    router.post(config.passwordAssign.endpoint, manageUserController.assignPassword)
    
  if (config.passwordInit && config.passwordInit.mount)
    router.post(config.passwordInit.endpoint, manageUserController.initPassword) 
  
  if (config.passwordChange && config.passwordChange.mount)
    router.post(config.passwordChange.endpoint, manageUserController.changePassword)
    
  if (config.regenerateConfirmKey && config.regenerateConfirmKey.mount)
    router.post(config.regenerateConfirmKey.endpoint, manageUserController.regenerateConfirmKey)
    
  if (config.confirm && config.confirm.mount)
    router.get(config.confirm.endpoint, manageUserController.verifyAccount)
    
  if (config.getInfoUserByUsername && config.getInfoUserByUsername.mount)
    router.get(config.getInfoUserByUsername.endpoint, manageUserController.getInfoUserByUsername)
    
  if (config.getAllUsers && config.getAllUsers.mount)
    router.get(config.getAllUsers.endpoint, manageUserController.getAllUsers)
  
  if (config.updateUser && config.updateUser.mount)
    router.post(config.updateUser.endpoint, manageUserController.updateUser)
  
  if (config.createUser && config.createUser.mount)
    router.post(config.createUser.endpoint, manageUserController.createUser)
  
  if (config.deleteUser && config.deleteUser.mount)
    router.delete(config.deleteUser.endpoint, manageUserController.deleteUser)

  router.get('/health', todo)
  router.get('/*', todo)
  return router
}

module.exports = init

// eslint-disable-next-line no-unused-vars
function todo(req, res) {
  return res.status(400).json({
    msg: 'Not implemented'
  })
}

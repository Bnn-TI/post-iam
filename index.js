const {
    createApi,
    defaultConfig,
} = require('./lib/microgi-api')
const createExpressMiddleware = require('./lib/express-middleware')
const debug = require('debug')('microgi:iam:api')

async function initApi(app, config) {
    const iamConfig = config === null
        ? defaultConfig 
        : { ...defaultConfig, ...config}
    const api = createApi(app, iamConfig)
    await api.middleware()
    return api
}

/**
 * Creates an Express Middleware
 * @param {Express} app 
 * @param {object} config 
 */
initApi.expressMiddleware = async (app, config) => {
    const iamConfig = config === null
        ? defaultConfig 
        : { ...defaultConfig, ...config}
    debug('IAM Config', JSON.stringify(iamConfig,null,2))
    return createExpressMiddleware(app, iamConfig)
}

exports = module.exports = initApi

const debug = require('debug')('microgi:iam:api:services')
const mongo = require('mongodb').MongoClient
const MONGODB_URI = process.env.MONGODB_URI || 'mongodb://localhost:27017'
const DB_NAME = process.env.DB_NAME || 'microgi_iam'
const DATASOURCE_NAME = process.env.DATASOURCE_NAME || 'microgi_iam'

const users = require('./users')

class Api {
  constructor() {}

  async init(app) {
    if (
      app &&
      app.dataSources &&
      Object.keys(app.dataSources).length > 0 &&
      app.dataSources[DATASOURCE_NAME] &&
      app.dataSources[DATASOURCE_NAME].connector
    ) {
      try {
        debug('Connecting throught app connector')
        this.db = await connectToAppDataSource(app)
        this.users = await users(this.db)
        debug('ended init api')
      } catch (err) {
        throw err
      }
    } else {
      debug('Connecting throught default connector')
      this.client = await mongo.connect(MONGODB_URI, {
        useNewUrlParser: true,
        useUnifiedTopology: true
      })
      this.db = this.client.db(DB_NAME)
      this.users = await users(this.db)
    }
  }
}

const createProgramaticApi = async function(app) {
  const programaticApi = new Api()
  await programaticApi.init(app)
  return programaticApi
}

const connectToAppDataSource = async function(app) {
  debug('trying to connect to app datasource')
  let promise = await new Promise((resolve, reject) => {
    this.dataSource = app.dataSources[DATASOURCE_NAME].connector
    this.dataSource.connect((err, db) => {
      debug('app datasource connection success')
      if (err) {
        debug('Error in connection', err)
        const err = new Error('Error in connection')
        err.message = 'Error in connection'
        reject(err)
      }
      resolve(db)
    })
  }).catch(err => {
    debug('Error in promise', err)
    throw err
  })
  return promise
}

module.exports = createProgramaticApi

const bcrypt = require('bcrypt')
const ObjectId = require('mongodb').ObjectId
const debug = require('debug')('microgi:iam:api:microgi-api:services:users')
const USERS_COLLECTION = process.env.USERS_COLLECTION || 'iam_users'
const DEFAULT_USER_REALM = process.env.DEFAULT_USER_REALM || 'iam_default_realm'
const ACCESS_TOKENS_COLLECTION =
  process.env.ACCESS_TOKENS_COLLECTION || 'iam_access_tokens'
const REFRESH_TOKENS_COLLECTION =
  process.env.REFRESH_TOKENS_COLLECTION || 'iam_refresh_tokens'
const DEFAULT_PASSWORD = process.env.DEFAULT_PASSWORD || 'password'
const useObjectId = process.env.USE_OBJECTID === 'true'

const {
  validateValidityForToken,
  errorCodes,
  isNotDefined,
  generateTokenUser
} = require('../../utils/security')

const { getQueryForDatesOperator, formatDataUsers } = require('../../utils/general')

const defaultProject = {
  username: true,
  roles: true,
  createdOn: true,
  active: true,
  verified: true,
  currentLoginOn: true,
  lastLoginOn: true
}

function generateQuery(query, where, operator) {

  if (operator) {
    where[operator].map((rule) => {
      if (rule.createdOn || rule.currentLoginOn || rule.lastLoginOn) {
        if (!query) return null
        query = getQueryForDatesOperator(query, rule, `$${operator}`)
      } else if (rule.username) {
        if (!query) return null
        query[`$${operator}`].push({ username: `${DEFAULT_USER_REALM}:${rule.username}` })
      } else {
        if (!query) return null
        query[`$${operator}`].push(rule)
      }
    })
  } else {
    if (where.createdOn || where.currentLoginOn || where.lastLoginOn) {
      if (!query) return null
      query = getQueryForDatesOperator(query, where)
    } else if (where.username) {
      if (!query) return null
      query = { username: `${DEFAULT_USER_REALM}:${where.username}` }
    } else {
      if (!query) return null
      query = where
    }

  }
  query.deletedAt = null
  return query
}

class Users {
  constructor(db) {
    this.db = db
  }

  async registerUser(user) {
    let {
      username,
      roles,
      password,
      active,
      verified,
    } = user

    debug(`with username: ${username}`)
    if (username.indexOf(':') <= 0) {
      debug('there is no realm for this user, default realm will be appended')
      username = `${DEFAULT_USER_REALM}:${username.replace(/:/)}`
    }

    debug(`looking for user ${username} ${USERS_COLLECTION}`)
    const col = this.db.collection(USERS_COLLECTION)
    const count = await col.countDocuments({
      username
    })

    if (count > 0) return { data: false, error: errorCodes.DUPLICATED }

    let passwordHash = null
    if (password) passwordHash = bcrypt.hashSync(password, 10)
    else passwordHash = bcrypt.hashSync(DEFAULT_PASSWORD, 10)

    let token = null
    if (!verified) token = await generateTokenUser()

    
    const insertData = {
      username: username,
      roles: roles,
      password_hash: passwordHash,
      createdOn: new Date(),
      createdBy: user.user,
      passwordKey: null,
      confirmKey: token,
      active: active ? true : false,
      verified: verified ? true : false
    }

    if(useObjectId && ObjectId.isValid(user.user)) insertData.createdBy = ObjectId(user.user)

    const result = await col.insertOne(insertData)

    if (!result) {
      debug('save incorrectly')
      return { data: false, error: active ? errorCodes.CREATE : errorCodes.UPDATE }
    }

    debug('save token success')

    let data = null
    if (verified) data = { id: result.insertedId, username }
    else data = { id: result.insertedId, username, confirmKey: token.data }

    return { data, error: null }
  }

  /**
   *
   * @param {object} agentUp
   * @param {boolean} action
   */
  async updateUser(agentUp, action) {
    const Users = this.db.collection(USERS_COLLECTION)
    const username = `${DEFAULT_USER_REALM}:${agentUp.username.replace(/:/)}`
    const user = await Users.findOne({
      username: username
    })

    const Token = this.db.collection(ACCESS_TOKENS_COLLECTION)
    const RefreshToken = this.db.collection(REFRESH_TOKENS_COLLECTION)
    let save = null
    let token = null
    let refreshToken = null
    if (action) {
      debug('baja de usuario', username)
      save = await Users.updateOne(
        {
          _id: user['_id']
        },
        {
          $set: {
            deletedAt: new Date(),
            active: false
          }
        }
      )
      token = await Token.deleteMany({
        username: username
      })
      refreshToken = await RefreshToken.deleteMany({
        username: username
      })
    } else {
      debug('actualiza acceso', username)
      const data = {
        $set: {
          roles: agentUp.roles,
          active: agentUp.active,
          deletedAt: null
        }
      }

      if(agentUp.password) data['$set']['password_hash'] = bcrypt.hashSync(agentUp.password, 10)
      
      save = await Users.updateOne(
        {
          _id: user['_id']
        }, data
      )

      debug('Actualiza tokens', agentUp.active)
      if (!agentUp.active) {
        token = await Token.deleteMany({
          username: username
        })
        refreshToken = await RefreshToken.deleteMany({
          username: username
        })
      }
    }
    if (!save) {
      debug('save incorrectly')
      return { data: false, error: errorCodes.UPDATE }
    }
    debug('user updated')
    return { data: { user }, error: null }
  }

  /**
   * Regenerate Confirm Key
   * @param {string} username
   */
  async regenerateConfirmKey(username) {
    const Users = this.db.collection(USERS_COLLECTION)
    debug(`with username: ${username}`)
    if (username.indexOf(':') <= 0) {
      debug('there is no realm for this user, default realm will be appended')
      username = `${DEFAULT_USER_REALM}:${username.replace(/:/)}`
    }

    debug(`looking for user ${username} ${USERS_COLLECTION}`)
    const user = await Users.findOne({
      username: username
    })

    if (user) {
      if (user.active) {
        if (!user.verified) {
          const token = await generateTokenUser()
          const result = await Users.updateOne(
            {
              _id: user['_id']
            },
            {
              $set: {
                confirmKey: token
              }
            }
          )
          if (!result) {
            debug('save incorrectly')
            return { data: false, error: errorCodes.UPDATE }
          }
          debug('save token success')
          return { data: { username, confirmKey: token.data }, error: null }
        } else {
          debug('user verified')
          return { data: false, error: errorCodes.password.VERIFIED }
        }
      } else {
        debug('user not active')
        return { data: false, error: errorCodes.password.INACTIVE }
      }
    } else {
      debug('user not exist')
      return { data: false, error: errorCodes.NOT_FOUND }
    }
  }

  /**
   * Verify new account
   * @param {string} username
   * @param {string} confirmKey
   */
  async verifyAccount(username, confirmKey) {
    try {
      debug(`1-verifyAccount ${username} -- ${confirmKey}`)
      const Users = this.db.collection(USERS_COLLECTION)
      const user = await Users.findOne({
        $and: [{ username }, { verified: false }, { active: true }]
      })
      if (user) {
        const isValid = validateValidityForToken(
          process.env.VALIDITY_ACCESS_KEY_LIVE,
          user.confirmKey.created,
          new Date()
        )
        debug(`isValid ${isValid} -- ${user.confirmKey.created}`)
        if (isValid) {
          debug('user exist')
          const token = await generateTokenUser()
          debug('token', token)
          if (!token) {
            debug('token generate incorrectly')
            return { data: false, error: errorCodes.UPDATE }
          }
          debug('token generate correctly')
          const save = await Users.updateOne(
            {
              _id: user['_id']
            },
            {
              $set: {
                passwordKey: token,
                confirmKey: null,
                verified: true
              }
            }
          )
          if (!save) {
            debug('save incorrectly')
            return { data: false, error: errorCodes.UPDATE }
          }

          debug('save token success')
          return { data: { username, token: token.data }, error: null }
        } else {
          debug('expired confirm key')
          return { data: false, error: errorCodes.password.KEY_INVALID }
        }
      } else {
        debug('user not exist')
        return { data: false, error: errorCodes.NOT_FOUND }
      }
    } catch (error) {
      debug('error interno', error)
      return { data: false, error: 'INIT_ERROR' }
    }
  }

  /**
   * init flow of restore passsword for an user
   * @param {string} username
   */
  async initPassword(username) {
    try {
      const Users = this.db.collection(USERS_COLLECTION)
      const user = await Users.findOne({
        $and: [{ username }, { verified: true }, { active: true }]
      })
      if (user) {
        debug('user exist')
        const token = await generateTokenUser(true)
        debug('token', token)
        if (!token) {
          debug('token generate incorrectly')
          return { data: false, error: 'TOKEN_ERROR' }
        }

        debug('token generate correctly')
        await Users.updateOne(
          {
            _id: user['_id']
          },
          {
            $set: {
              passwordKey: token
            }
          }
        )
        debug('save token success')
        return { data: { token, user }, error: null }
      } else {
        const user = await Users.findOne({
          $and: [{ username }, { verified: false }]
        })
        if (user) {
          if (user.active) {
            debug('user not verified')
            return { data: false, error: errorCodes.password.NOT_VERIFIED }
          } else {
            debug('user not active')
            return { data: false, error: errorCodes.password.INACTIVE }
          }
        } else {
          debug('user not exist')
          return { data: false, error: errorCodes.NOT_FOUND }
        }
      }
    } catch (e) {
      debug('error interno', e)
      return { data: false, error: errorCodes.UPDATE }
    }
  }

  /**
   * change passsword an user
   * @param {string} username
   * @param {string} password
   * @param {string} passwordConfirm
   */
  async changePassword(username, password, passwordConfirm, passwordCurrently) {
    try {
      debug('process change password')
      return this.processSavePassword(
        username,
        null,
        password,
        passwordConfirm,
        passwordCurrently,
        true
      )
    } catch (e) {
      debug('error', e)
      return { data: false, error: errorCodes.UPDATE }
    }
  }

  /**
   *
   * @param {string} username
   * @param {string} key
   * @param {string} password
   * @param {string} passwordConfirm
   */
  async assignPassword(username, key, password, passwordConfirm) {
    try {
      debug('process assign password')
      return await this.processSavePassword(
        username,
        key,
        password,
        passwordConfirm
      )
    } catch (e) {
      debug('error', e)
      return { data: false, error: errorCodes.UPDATE }
    }
  }
  /**
   * validate rules for user
   * @param {string} user
   * @param {string} key
   * @param {string} password
   * @param {string} passwordConfirm
   */

  validationsOfPasswordUser(
    user,
    key,
    password,
    passwordConfirm,
    passwordCurrently,
    change
  ) {
    const validate = this.validatePasswordCNSF(password, user)
    debug('validate', validate)
    if (validate) {
      return validate
    }

    if (password !== passwordConfirm) {
      debug('password and password confirm are no the same')
      return errorCodes.password.SAME_PASSWORDS
    }

    if (isNotDefined(user)) {
      debug('user username is not verified  or not register')
      return errorCodes.password.NOT_REGISTER
    }

    if (!user.active) {
      debug('user is inactive')
      return errorCodes.password.INACTIVE
    }

    if (!change) {
      if (isNotDefined(user.passwordKey)) {
        debug('user password key is not defined')
        return errorCodes.password.KEY_INVALID
      }
      if (user.passwordKey.data !== key) {
        debug('user key is not correctly')
        return errorCodes.password.KEY_INVALID
      }

      if (user.password_hash) {
        if (key.substring(0, 2) === process.env.RESTORE_PASSWORD) {
          if (bcrypt.compareSync(password, user.password_hash)) {
            debug('password new is the same that previous one')
            return errorCodes.password.SAME_PASSWORD
          }
        }
      }

      if (
        !validateValidityForToken(
          process.env.VALIDITY_PASSWORD_KEY_LIVE,
          user.passwordKey.created,
          new Date()
        )
      ) {
        debug('validity password key expired')
        return errorCodes.EXPIRED
      }
    }

    if (change) {
      if (bcrypt.compareSync(password, user.password_hash)) {
        debug('password new is the same that previous one')
        return errorCodes.password.SAME_PASSWORD
      }

      if (passwordCurrently) {
        if (bcrypt.compareSync(passwordCurrently, user.password_hash))
          debug('passwordcurrently is the same that stored in DB')
        else {
          debug('passwordcurrently is not the same that stored in DB')
          return errorCodes.password.FORBIDDEN_PASSWORD
        }
      }
    }
  }

  /**
   *
   * @param {string} value
   * @param {object} user
   */

  validatePasswordCNSF(value, user) {
    const expRegTwoCharConsecutive = new RegExp(
      'abc|bcd|cde|def|efg|fgh|ghi|hij|ijk|jkl|klm|lmn|mno|nop|opq|pqr|qrs|rst|stu|tuv|uvw|vwx|wxy|xyz|ABC|BCD|CDE|DEF|EFG|FGH|GHI|HIJ|IJK|JKL|KLM|LMN|MNO|NOP|OPQ|PQR|QRS|RST|STU|TUV|UVW|VWX|WXY|XYZ'
    )
    const expRegTwoSameCharConsecutive = new RegExp(
      'aaa|bbb|ccc|ddd|eee|fff|ggg|hhh|iii|jjj|kkk|lll|mmm|nnn|ooo|ppp|qqq|rrr|sss|ttt|uuu|vvv|www|xxx|yyy|zzz|AAA|BBB|CCC|DDD|EEE|FFF|GGG|HHH|III|JJJ|KKK|LLL|MMM|NNN|OOO|PPP|QQQ|RRR|SSS|TTT|UUU|VVV|WWW|XXX|YYY|ZZZ'
    )
    const expRegConsecutiveNumbers = new RegExp(
      '(012|123|234|345|456|567|678|789)'
    )
    const expRegNumeric = new RegExp('[0-9]+')
    const expRegChar = new RegExp('[a-zA-Z]+')
    const expRegEmail = new RegExp('.+@*.com')
    const expRegConsecutiveEspecialCharsEquals = new RegExp('(\\W|_)\\1\\1')
    const username = user.username.split(':')[1].split('@')[0]
    const expRegUsername = new RegExp(username)

    if (value && expRegUsername.test(value)) {
      return errorCodes.password.USERNAME_ERROR
    }

    // valida el correo electronico
    if (value && expRegEmail.test(value)) {
      return errorCodes.password.EMAIL_ERROR
    }

    if (value && !expRegNumeric.test(value)) {
      return errorCodes.password.CHARS_ERROR
    }

    if (value && !expRegChar.test(value)) {
      return errorCodes.password.LEAST_A_LETER
    }
    // valida que el password la longitud del password sea mayor a 8
    if (value && value.length < 8) {
      return errorCodes.password.MIN_LENGTH
    }

    // valida que el password no tenga mas de dos letras consecutivas
    if (value && expRegTwoCharConsecutive.test(value)) {
      return errorCodes.password.UNTIL_TWO_LETTER
    }
    // valida que el password no tenga mas de dos letras consecutivas iguales
    if (value && expRegTwoSameCharConsecutive.test(value)) {
      return errorCodes.password.UNTIL_TWO_SAME_LETTER
    }
    // valida que el password no tenga mas de 4 digitos consecutivos
    if (value && expRegConsecutiveNumbers.test(value)) {
      return errorCodes.password.UNTIL_TWO_NUMBER
    }
    // valida que el password no tenga mas de dos caracteres especiales consecutivos
    if (value && expRegConsecutiveEspecialCharsEquals.test(value)) {
      return errorCodes.password.UNTIL_TWO_SPECIAL_CHARS
    }
  }
  /**
   * @param {string} username
   * @param {string} key
   * @param {string} password
   * @param {string} passwordConfirm
   * @param {boolean} change
   */

  async processSavePassword(
    username,
    key,
    password,
    passwordConfirm,
    passwordCurrently,
    change
  ) {
    try {
      const Users = this.db.collection(USERS_COLLECTION)

      debug('username', username)
      const user = await Users.findOne({
        $and: [{ username }, { verified: true }]
      })

      debug('user', user)
      if (user) {
        const validationUser = this.validationsOfPasswordUser(
          user,
          key,
          password,
          passwordConfirm,
          passwordCurrently,
          change
        )
        if (validationUser) {
          debug('password not pass all validation of user')
          return { data: false, error: validationUser }
        }
        debug('pass validation user', validationUser)
        const passwordHash = bcrypt.hashSync(
          password,
          parseInt(process.env.SALT_ROUNDS)
        )
        debug('passwordhash', passwordHash)
        if (!passwordHash) {
          debug('password not pass validation of user')
          return { data: false, error: validationUser }
        }
        debug('password hash generate correctly')
        await Users.updateOne(
          {
            _id: user['_id']
          },
          {
            $set: {
              password_hash: passwordHash,
              passwordKey: null
            }
          }
        )
        debug('password save success')
        return { data: { user, key, passwordHash }, error: null }
      } else {
        debug('user not found')
        return { data: false, error: errorCodes.NOT_FOUND }
      }
    } catch (e) {
      debug('Ocurrio un error en el proceso de guardado', e)
      return { data: false, error: errorCodes.UPDATE }
    }
  }
  /**
   *
   * @param {*} username
   */
  async getInfoUserByUsername(username) {
    try {
      const Users = this.db.collection(USERS_COLLECTION)
      const user = await Users.findOne({
        username: username
      })
      const data = {
        id: user._id,
        username: user.username,
        currentLoginOn: user.currentLoginOn,
        lastLoginOn: user.lastLoginOn,
        roles: user.roles,
        active: user.active,
        verified: user.verified
      }
      return { data, error: null }
    } catch (e) {
      debug('error al realizar la consulta', e)
      return { data: false, error: errorCodes.INTERNAL }
    }
  }

  /**
   *
   * Elimina un token de BD a partir del nombre de usuario y del token
   *
   * @param {*} username Nombre de usuario
   * @param {*} token Token a ser eliminado
   */
  async deleteTokenByUsernameAndToken(username, token) {
    try {
      const Tokens = this.db.collection(ACCESS_TOKENS_COLLECTION)
      const deletedToken = await Tokens.deleteOne({
        username,
        token
      })

      return { data: deletedToken, error: null }
    } catch (e) {
      debug('error al eliminar el token', e)
      return { data: false, error: errorCodes.INTERNAL }
    }
  }

  /**
     * 
     * @param {*} void 
     */
  async getAllUsers(queryParam) {
    try {
      const Users = this.db.collection(USERS_COLLECTION)
      let fields = defaultProject

      let query = {deletedAt: null}
      debug('queryParam', queryParam)
      if (queryParam && queryParam.filter) {
        let filter = JSON.parse(queryParam.filter)
        debug('generate filter')
        if (filter.where) {
          if (filter.where.and) {
            query.$and = []
            query = generateQuery(query, filter.where, 'and')
            debug('generate and')
          } else if (filter.where.or) {
            query.$or = []
            query = generateQuery(query, filter.where, 'or')
            debug('generate or')
          } else {
            query = generateQuery(query, filter.where)
          }
        }

        if (filter.fields && Array.isArray(filter.fields) && filter.fields.length > 0) {
          const validKeys = Object.keys(defaultProject)
          const project = {}
          filter.fields.map((field) => {
            if (validKeys.includes(field)) project[field] = true
          })
          debug('generate projects')
          fields = project
        }
      }

      debug('query generate', query)

      if (!query) return { data: null, error: errorCodes.filter.SYNTAX_ERROR }

      const pipeline = []
      const options = {}
      
      fields.createdByUser = { username: true}
      
      pipeline.push(
      {
        $lookup: {
          from: USERS_COLLECTION,
          localField: 'createdBy',
          foreignField: '_id',
          as: 'createdByUser',
        }
      },
      {
        $match : query
      },{
        $project: fields
      })

      

      options.allowDiskUse = true;
      options.explain = false;
      
      const usersData = await Users.aggregate(pipeline, options)

      const data = await usersData.toArray()

      return { data: formatDataUsers(data), error: null }
    } catch (e) {
      debug('error al realizar la consulta', e)
      return { data: null, error: errorCodes.INTERNAL }
    }
  }
}

const createUsers = async function (db) {
  const usersApi = new Users(db)
  return usersApi
}

module.exports = createUsers
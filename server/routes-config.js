const config = {
    endpoint: '',
    mount: true,
    token: {
        before: async function (params, req, res, next) { },
        endpoint: '/token',
        mount: true,
        after: async function (params, req, res, next) { },
    },
    logout: {
        before: async function (params, req, res, next) { },
        endpoint: '/logout',
        mount: true,
        after: async function (params, req, res, next) { },
    },
    regenerateConfirmKey: {
        before: async function (params, req, res, next) { },
        endpoint: '/regenerateConfirmKey',
        mount: true,
        after: async function (params, req, res, next) { },
    },
    confirm: {
        before: async function (params, req, res, next) { },
        endpoint: '/verifyAccount',
        mount: true,
        after: async function (params, req, res, next) { },
    },
    passwordAssign: {
        before: async function (params, req, res, next) { },
        endpoint: '/assignPassword',
        mount: true,
        after: async function (params, req, res, next) { },
    },
    passwordInit: {
        before: async function (params, req, res, next) { },
        endpoint: '/initPassword',
        mount: true,
        after: async function (params, req, res, next) { },
    },
    passwordChange: {
        before: async function (params, req, res, next) { },
        endpoint: '/changePassword',
        mount: true,
        after: async function (params, req, res, next) { },
    },
    getInfoUserByUsername: {
        before: async function (params, req, res, next) { },
        endpoint: '/getInfoUserByEmail',
        mount: true,
        after: async function (params, req, res, next) { },
    },
    getAllUsers: {
        before: async function (params, req, res, next) { },
        endpoint: '/getAllUsers',
        mount: true,
        after: async function (params, req, res, next) { },
    },
    createUser: {
        before: async function (params, req, res, next) { },
        endpoint: '/createUser',
        mount: true,
        after: async function (params, req, res, next) { },
    },
    updateUser: {
        before: async function (params, req, res, next) { },
        endpoint: '/updateUser',
        mount: true,
        after: async function (params, req, res, next) { },
    },
    deleteUser: {
        before: async function (params, req, res, next) { },
        endpoint: '/deleteUser',
        mount: true,
        after: async function (params, req, res, next) { },
    }
}

module.exports = config
/**
 * Verify is value is an valid object
 * @param {object} object 
 */

const isObject = function (object){
    let objectContructor = ({}).constructor

    if(object && object !== null){
        if (object.constructor === objectContructor) {
            const keys = Object.keys(object)
            if(keys.length > 0)
            return keys
        }
    }

    return null
}
 
/**
 * Generates a query through operator passed
 * @param {object} query 
 * @param {object} rule 
 * @param {string} operator 
 */
const getQueryForDatesOperator = function(query, rule, operator){
  const index = Object.keys(rule)[0]
  const isObj = isObject(rule[index])
    if(operator){
      if (isObj){
        if(isObj.length > 1)
          return null  

        query[operator].push({[index]: {[isObj]: new Date(rule[index][isObj])}}) 
      } else {
        query[operator].push({[index]: new Date(rule[index])})
      }
    } else{
      if (isObj){
        if(isObj.length > 1)
          return null  
        const key = Object.keys(rule)[0]
        query = {[index]: {[isObj[0]]: new Date(rule[index][key])}}
      }else{
        query = {[index]: new Date(rule[index])}
      }
    }
    
    return query
}

const formatDataUsers = function(data){
  let results = []

  if(Array.isArray(data) && data.length > 0){
    results = data.map((item)=>{
      if(Array.isArray(item.createdByUser) && item.createdByUser.length > 0)
        item.createdByUser = item.createdByUser[0]
      else item.createdByUser = null

      return item
    })
  }
    
  return results
}

exports.isObject = isObject
exports.getQueryForDatesOperator = getQueryForDatesOperator
exports.formatDataUsers = formatDataUsers


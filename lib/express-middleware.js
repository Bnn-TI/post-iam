const debug = require('debug')('microgi:iam:express')
const express = require('express')
const cookieParser = require('cookie-parser')
const programaticApi = require('./services')
const user = require('../server/routes')
const admin = require('../server/admin-routes')
const security = require('../security')

const COOKIE_SECRET_KEY = process.env.COOKIE_SECRET_KEY

const router = express.Router()

/**
 * Creates express middleware
 * @param {Express} app 
 * @param {object} config 
 */
async function createExpressMiddleware(app, config){
  let pApi
  try{
    pApi = await programaticApi(app)
  }catch(e) {
    throw e
  }
  
  debug('Creating express middleware')
  if (!COOKIE_SECRET_KEY) {
    throw new Error('COOKIE_SECRET_KEY not configured')
  }
  //Parses iam cookie
  app.use(cookieParser(COOKIE_SECRET_KEY))

  //Add IAM instances to each request
  app.use((req, res, next) => {
    res.locals.db = pApi.db
    res.locals.microgiConfig = config
    res.locals.microgiApi = { pApi }
    next()
  })

  app.use(security(config.security))

  if (config.methods.user.mount) {
    router.use(
      config.methods.user.endpoint,
      user(config.methods.user)
    )
  }
  if (config.methods.admin.mount) {
    router.use(
      config.methods.admin.endpoint,
      admin(config.methods.admin)
    )
  }
  return router
}

module.exports = createExpressMiddleware
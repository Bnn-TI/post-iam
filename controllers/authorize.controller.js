const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')
const debug = require('debug')('microgi:iam:controllers:authorize')

const { invalidCredentials, unauthorized } = require('./errors')
const {
  jwtSignSecret,
  generateCookies,
  clearCookies
} = require('../utils/security')

const USERS_COLLECTION = process.env.USERS_COLLECTION || 'iam_users'
const CLIENTS_COLLECTION = process.env.CLIENTS_COLLECTION || 'iam_clients'
const ACCESS_TOKENS_COLLECTION =
  process.env.ACCESS_TOKENS_COLLECTION || 'iam_access_tokens'
const REFRESH_TOKENS_COLLECTION =
  process.env.REFRESH_TOKENS_COLLECTION || 'iam_refresh_tokens'
const DEFAULT_ACCESS_TOKEN_EXPIRATION =
  Number.parseInt(process.env.DEFAULT_ACCESS_TOKEN_EXPIRATION || '1000') * 1000
const DEFAULT_REFRESH_TOKEN_EXPIRATION =
  Number.parseInt(process.env.DEFAULT_REFRESH_TOKEN_EXPIRATION || '1000') * 1000
const RETURN_ACCESS_TOKEN_IN_COOKIE =
  process.env.RETURN_ACCESS_TOKEN_IN_COOKIE || 'false'
const ACCESS_TOKEN_COOKIE_NAME =
  process.env.ACCESS_TOKEN_COOKIE_NAME || 'microgi_cookie'
const ALLOW_SIMULTANEOUS_ACCESS =
  process.env.ALLOW_SIMULTANEOUS_ACCESS || 'true'
const ALLOW_MULTIPLE_TOKENS = process.env.ALLOW_MULTIPLE_TOKENS || 'true'
const COOKIE_SECRET_KEY = process.env.COOKIE_SECRET_KEY
const LOGGED_IN_COOKIE_NAME =
  process.env.LOGGED_IN_COOKIE_NAME || 'microgi_cookie_logged_in'

/**
 *
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
exports.authorize = async function(req, res, next) {
  const { body } = req

  switch (body['grant_type']) {
  case 'password':
    return authorizeWithPassword(req, res, next)
  default:
    return next({
      status: 409,
      code: 'GRANT_TYPE_NOT_IMPLEMENTED',
      message: `grant ${body['grant_type']} is not implemented`
    })
  }
}

/**
 * Creates authorization object for user with password
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
async function authorizeWithPassword(req, res, next) {
  try {
    res.header('Access-Control-Allow-Origin', req.headers.origin)
    if (res.locals.microgiConfig.methods.user.token.before) {
      const params = { username: req.body.username }
      await res.locals.microgiConfig.methods.user.token.before(
        params,
        req,
        res,
        next
      )
    }

    const { db } = res.locals

    debug('load client')
    const client = await loadClient(req, res)
    if (!client) return invalidCredentials(next)

    debug('validating user has realm')
    const { username } = req.body
    if (username && username.indexOf(':') <= 0)
      return invalidCredentials(next, 'a realm is required')

    debug('load user')
    const user = await loadUser(req, res)
    if (!user) return invalidCredentials(next)
    if (!user.active) return unauthorized(next, 'USER_NOT_ACTIVE')

    const tokens = db.collection(ACCESS_TOKENS_COLLECTION)
    const actualToken = await tokens.findOne({
      username: user.username,
      clientId: client._id
    })

    if (
      ALLOW_SIMULTANEOUS_ACCESS &&
      ALLOW_SIMULTANEOUS_ACCESS.toLocaleLowerCase().trim() === 'false'
    ) {
      if (actualToken) {
        return unauthorized(next, 'USER_ALREADY_LOGGED_IN')
      }
    }

    let tokenResult = actualToken
    let token = null
    let refreshToken = null
    const expireAt = new Date(Date.now() + DEFAULT_ACCESS_TOKEN_EXPIRATION)
    let keyId = null

    if (
      (ALLOW_MULTIPLE_TOKENS &&
        ALLOW_MULTIPLE_TOKENS.toLocaleLowerCase().trim() === 'true') ||
      null == actualToken
    ) {
      debug('creating token document')
      tokenResult = await tokens.insertOne({
        username: user.username,
        clientId: client._id
      })

      keyId = tokenResult.ops[0]._id

      debug('creating token')
      token = await createAccessToken(
        user.username,
        client._id,
        user.roles,
        keyId.toString()
      )

      debug('updating token document with generated token')
      await tokens.updateOne(
        {
          _id: keyId
        },
        {
          $set: {
            token,
            expiresIn: DEFAULT_ACCESS_TOKEN_EXPIRATION,
            createdOn: new Date(),
            expireAt: expireAt
          }
        }
      )

      debug(`creating refresh token for ${keyId}`)
      refreshToken = await createRefreshToken(user.username, client._id, keyId)

      debug('persisting refresh token')
      await saveRefreshToken(db, user.username, client._id, refreshToken, keyId)
    } else {
      keyId = actualToken._id
      const refreshTokenDB = await db
        .collection(REFRESH_TOKENS_COLLECTION)
        .findOne({
          accessTokenId: keyId
        })
      if (refreshTokenDB) refreshToken = refreshTokenDB.token
      else {
        debug(`creating refresh token for ${keyId}`)
        refreshToken = await createRefreshToken(
          user.username,
          client._id,
          keyId
        )

        debug('persisting refresh token')
        await saveRefreshToken(
          db,
          user.username,
          client._id,
          refreshToken,
          keyId
        )
      }
      token = actualToken.token
    }

    debug('updating last login user')
    await lastLoginForUser(db, user)

    debug('sending results')

    if (
      RETURN_ACCESS_TOKEN_IN_COOKIE &&
      RETURN_ACCESS_TOKEN_IN_COOKIE.toLocaleLowerCase().trim() !== 'false'
    ) {
      const cookieValue = token
      generateCookies(cookieValue, expireAt, req.headers, res)
    }

    if (res.locals.microgiConfig.methods.user.token.after) {
      const params = { username, token, user }
      await res.locals.microgiConfig.methods.user.token.after(
        params,
        req,
        res,
        next
      )
    }

    res.send({
      access_token: token,
      refresh_token: refreshToken,
      type: 'bearer',
      expires_in: DEFAULT_ACCESS_TOKEN_EXPIRATION,
      roles: user.roles,
      keyId
    })
  } catch (e) {
    next(e)
  }
}

/**
 *
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
exports.logout = async function(req, res, next) {
  try {
    let token = null
    const authHeader = req.headers['authorization']
    if (authHeader) {
      debug('extracting token from authorization header')
      let destructuredHeader = authHeader.split(' ')
      if (
        destructuredHeader[0].toLocaleLowerCase() != 'bearer' ||
        destructuredHeader.length !== 2
      )
        return unauthorized(next, 'INVALID_TOKEN')
      token = destructuredHeader[1]
    }

    // Cookies that have not been signed
    debug('Cookies: ', req.cookies)
    // Cookies that have been signed
    debug('Signed Cookies: ', req.signedCookies)
    let cookie = COOKIE_SECRET_KEY
      ? req.signedCookies[ACCESS_TOKEN_COOKIE_NAME]
      : req.cookies[ACCESS_TOKEN_COOKIE_NAME]
    if (cookie) {
      debug('cookie', cookie)
      token = cookie
    } else {
      return unauthorized(next, 'INVALID_TOKEN')
    }

    debug('deleting token from db')
    const item = await res.locals.db
      .collection(ACCESS_TOKENS_COLLECTION)
      .deleteOne({
        token: token
      })

    clearCookies(req.headers, res)
    res.status(200).send({
      access_token: token
    })
  } catch (e) {
    next(e)
  }
}

/**
 *
 * @param {*} header
 */
function decodeAuthorization(header) {
  const decoded = String(
    Buffer.from(header.split(' ')[1], 'base64'),
    'UTF-8'
  ).split(':')
  return {
    clientId: decoded[0],
    clientSecret: decoded[1]
  }
}

/**
 *
 * @param {*} req
 * @param {*} res
 */
async function loadClient(req, res) {
  try {
    const { headers } = req
    const { db } = res.locals
    const { clientId, clientSecret } = decodeAuthorization(
      headers.authorization
    )
    const client = await db.collection(CLIENTS_COLLECTION).findOne({
      _id: clientId
    })
    if (!client) return null
    if (bcrypt.compareSync(clientSecret, client.secret_hash)) return client
  } catch (e) {
    throw e
  }
  return null
}

/**
 *
 * @param {*} req
 * @param {*} res
 */
async function loadUser(req, res) {
  try {
    const { body } = req
    const { username, password } = body
    const { db } = res.locals
    const user = await db.collection(USERS_COLLECTION).findOne({
      username,
      deletedAt: null
    })
    if (!user) return null
    if (bcrypt.compareSync(password, user.password_hash)) return user
  } catch (e) {
    throw e
  }
  return null
}

async function lastLoginForUser(db, user) {
  await db.collection(USERS_COLLECTION).updateOne(
    {
      _id: user._id
    },
    {
      $set: {
        lastLoginOn: user.currentLoginOn,
        currentLoginOn: new Date()
      }
    }
  )
}

/**
 *
 * @param {*} username
 * @param {*} roles
 * @param {*} keyId
 */
async function createAccessToken(username, clientId, roles, keyId) {
  return jwt.sign(
    {
      sub: username,
      clientId: clientId,
      roles: roles
    },
    jwtSignSecret,
    {
      //issuer: process.env.JWT_TOKEN_ISSUER || 'microgi-iam',
      issuer: clientId || 'microgi-iam',
      // expiresIn: DEFAULT_ACCESS_TOKEN_EXPIRATION,
      keyid: keyId.toString(),
      algorithm: process.env.JWT_ALGORITHM || 'HS512'
    }
  )
}

/**
 *
 * @param {*} username
 * @param {*} clientId
 * @param {*} keyId
 */
async function createRefreshToken(username, clientId, keyId) {
  return jwt.sign(
    {
      sub: username,
      clientId: clientId,
      accessTokenId: keyId.toString()
    },
    jwtSignSecret,
    {
      //issuer: process.env.JWT_TOKEN_ISSUER || 'microgi-iam',
      issuer: clientId || 'microgi-iam',
      //expiresIn: DEFAULT_REFRESH_TOKEN_EXPIRATION,
      algorithm: process.env.JWT_ALGORITHM || 'HS512'
    }
  )
}

/**
 *
 * @param {*} db
 * @param {*} username
 * @param {*} clientId
 * @param {*} token
 * @param {*} keyId
 */
async function saveRefreshToken(db, username, clientId, token, keyId) {
  return await db.collection(REFRESH_TOKENS_COLLECTION).insertOne({
    username: username,
    clientId: clientId,
    expiresIn: DEFAULT_REFRESH_TOKEN_EXPIRATION,
    createdOn: new Date(),
    expireAt: new Date(Date.now() + DEFAULT_REFRESH_TOKEN_EXPIRATION),
    token: token,
    accessTokenId: keyId
  })
}

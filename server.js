const Server = require('./server/')
const microgi = require('./index')
const debug = require('debug')('microgi:iam:server')
const {
    defaultConfig
} = require('./lib/microgi-api')

async function init() {
    const server = new Server(process.env.PORT || 3000)
    server.app.use(expressStackPrinter)
    const api = await microgi(server.app, defaultConfig)
    await server.start()
}

function stackPrinter(req, res, next) {
    debug('Printing Stack For', req.url)

    function printItem(item, prefix) {
        prefix = prefix || ''

        if (item.route) {
            debug(prefix, 'Route', item.route.path)
        } else if (item.name === '<anonymous>') {
            debug(prefix, item.name, item.handle)
        } else {
            debug(prefix, item.name, item.method ? '(' + item.method.toUpperCase() + ')' : '')
        }

        printSubItems(item, prefix + ' -')
    }

    function printSubItems(item, prefix) {
        if (item.name === 'router') {
            debug(prefix, 'MATCH', item.regexp)

            if (item.handle.stack) {
                item.handle.stack.forEach(function (subItem) {
                    printItem(subItem, prefix)
                })
            }
        }

        if (item.route && item.route.stack) {
            item.route.stack.forEach(function (subItem) {
                printItem(subItem, prefix)
            })
        }

        if (item.name === 'mounted_app') {
            debug(prefix, 'MATCH', item.regexp)
        }
    }

    req.app._router.stack.forEach(function (stackItem) {
        printItem(stackItem)
    })

    next()
}

function dummyStackPrinter(req, res, next) {
    debug('Printing Stack For', req.url)
    next()
}

let expressStackPrinter = dummyStackPrinter
if (process.env.STACK_PRINTER === 'true') {
    expressStackPrinter = stackPrinter
}

// Server init
init()
const config = {
    endpoint: '/admin',
    mount: true,
    users: {
        before: async function (params, req, res, next) {},
        endpoint: '/users',
        mount: true,
        after: async function (params, req, res, next) {},
    },
    clients: {
        before: async function (params, req, res, next) {},
        endpoint: '/clients',
        mount: true,
        after: async function (params, req, res, next) {},
    }
}

module.exports = config